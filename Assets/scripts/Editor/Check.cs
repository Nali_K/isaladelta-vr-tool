﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Main))]
public class Check : Editor {

	public override void OnInspectorGUI () {
		DrawDefaultInspector ();

		Main myScript = (Main)target;
		if (GUILayout.Button ("Check and Prepare Scene")) {


			myScript.CheckAndPrepare();
		}
	}

}
