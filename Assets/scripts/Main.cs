﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour {
    
    public int button = 0;
	public RoadMapHandler roadMap;

	public Transform myCam;
	public float doubleTap=0.3f;
	public float doubleTapTimer=-1;
    public float clickSize = 50; // this might be too small
	public Material matNormal; // normal material when part belongs to a previous stage
	public Material matHighlight; // highlight material when part belogs to current stage
	public UIhandler canvas;
	public Transform canvasAnchor;
	public UIhandler uiHandler;
	public PermanentUI permanentUI;
    Vector3 pos;
                          // Use this for initialization
	void Awake(){
		CheckAndPrepare ();
		roadMap.main = this;
		roadMap.uiHanlder = uiHandler;
		if (roadMap.matNormal == null) {
			roadMap.matNormal = matNormal;
		}
		if (roadMap.matHighlight == null) {
			roadMap.matHighlight = matHighlight;
		}
	}
    void Start ()
    {
        OVRTouchpad.Create();
        OVRTouchpad.TouchHandler += HandleTouchHandler;

    }

    void HandleTouchHandler(object sender, System.EventArgs e)
    {
        OVRTouchpad.TouchArgs touchArgs = (OVRTouchpad.TouchArgs)e;
        if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Left)
        {
			transform.position+=(myCam.forward* 3.0f * Time.deltaTime);
        }
        if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Right)
        {
			transform.position+=(myCam.forward* -3.0f * Time.deltaTime);
        }
		if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Down)
        {
			StageForward ();
        }
		if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Up)
        {
			StageBack ();
        }
		if (touchArgs.TouchType == OVRTouchpad.TouchEvent.SingleTap)
        {
			if (doubleTapTimer >=  0) {
				//doubleTapEvent
				canvas.ShowUp(canvasAnchor);
			} else {
				doubleTapTimer = doubleTap;
			}
			//transform.position+=(myCam.forward* 3.0f * Time.deltaTime);
        }
    }
    void ClickHappened()
    {
        Debug.Log("CLICK!");
    }

    // Update is called once per frame
    void Update()
	{	
		permanentUI.SetStage (roadMap.stage,roadMap.maxStage);	
		if (doubleTapTimer >= 0) {
			doubleTapTimer -= Time.deltaTime;
		}
		if (Input.GetButton ("Fire1")) {
			//canvas.ShowUp(canvasAnchor);
		}

        //blabla();        
        /*if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                Vector3 incomingVec = hit.point - gunObj.position;
                Vector3 reflectVec = Vector3.Reflect(incomingVec, hit.normal);
                Debug.DrawLine(gunObj.position, hit.point, Color.red);
                Debug.DrawRay(hit.point, reflectVec, Color.green);
            }
        }
        */
    }

    void blabla()
    {

        var speed = 3.5f;
        /*if (Input.GetMouseButtonDown(button))
        {
        if (Camera.current != null)
            {
               // pos = Input.mousePosition;
            float moveForward = 0.001f;// Input.GetAxis("Mouse X");
            float moveUpward = 0.0f;// Input.GetAxis("Mouse Y");
                Vector3 camVec = Camera.current.transform.TransformVector(new Vector3(0.0f, moveUpward, moveForward));
                Camera.current.transform.Translate(camVec);
            }
        }*/
        if (Input.GetKey(KeyCode.RightArrow))
        {
			transform.Translate(new Vector3(0, 0, 3.0f * Time.deltaTime));
			Debug.Log("CLICK!");
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(new Vector3(0, 0, -speed * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));
        }

        if (Input.GetMouseButtonUp(button))
        {
        }
    }
	public void StageBack(){
		roadMap.StageSet (roadMap.stage - 1);
	}
	public void StageForward(){
		roadMap.StageSet (roadMap.stage + 1);
	}
	public void CheckAndPrepare(){
		//destroy the standard camera
		Debug.ClearDeveloperConsole ();

		DestroyImmediate (GameObject.Find ("/Main Camera"));
		GameObject ring = GameObject.Find ("/GazePointerRing");

		bool error = false;
		if (ring == null) {
			Debug.LogError ("geen GazePointerRing gevonden. Voeg een GazePointerRing toe uit de map \"Assets/prefabs/plaats dit in de nieuwe Scene\" door het in de scene te slepen");
			error = true;
		}else{
			ring.GetComponent<OVRGazePointer>().cameraRig=GetComponentInChildren<OVRCameraRig>();
		}
		GameObject eventSystem = GameObject.Find ("/EventSystem");
		if (eventSystem == null) {
			Debug.LogError ("geen EventSystem gevonden. Voeg een EventSystem toe uit de map \"Assets/prefabs/plaats dit in de nieuwe Scene\" door het in de scene te slepen");
			error = true;

		} else {
			eventSystem.GetComponent<UnityEngine.EventSystems.OVRInputModule> ().rayTransform = myCam;
		}

		roadMap = GameObject.FindObjectOfType<RoadMapHandler> ();
		if (roadMap == null) {
			Debug.LogError ("Geen Roadmap gevonden. Voeg een roadmap toe en sleep het script \"RoadMapHanlder\" er op");
			error = true;

		} else {
			roadMap.AddPartHandlers ();
		}
		if (!error) {
			Debug.Log ("Alle objecten aanwezig, project kan worden gebuild!");
		}
	}
}
