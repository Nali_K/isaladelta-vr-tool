﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIhandler : MonoBehaviour {

	const int STATE_HIDDEN =0;
	const int STATE_SHOWING =1;
	const int STATE_APPEARING =2;
	const int STATE_DISAPPEARING =3;

	bool pointerIn =false;
	private float alpha=0;
	public float countDown=5f;// countdown untill disappearing;
	public GameObject stageButton;
	public GameObject context;
	private int stagebuttons=0;
	Graphic[] renderers;
	float[] alphas;
	public int state=STATE_HIDDEN;
	// Use this for initialization
	void Start () {
		renderers = GetComponentsInChildren<Graphic> ();
		alphas = new float [renderers.Length];

		for (int i = 0;i<renderers.Length;i++) {
			alphas[i]=renderers[i].color.a;
		
		}
	}
	
	// Update is called once per frame
	void Update () {
		switch (state) {
		case STATE_APPEARING:
			alpha += Time.deltaTime * 2f;
			if (alpha >= 1) {
				alpha = 1;
				state = STATE_SHOWING;
				countDown = 5f;
			}
			for (int i = 0;i<renderers.Length;i++) {
				renderers[i].color = new Color (1, 1, 1, alphas[i]*alpha);
			}
			break;

		case STATE_SHOWING:
			if (!pointerIn) {
				countDown -= Time.deltaTime;
			}
			if (countDown <= 0) {
				state = STATE_DISAPPEARING;
			}
			break;
		
		case STATE_DISAPPEARING:
			alpha -= Time.deltaTime * 2f;
			if (alpha <=0) {
				alpha = 0;
				transform.position = new Vector3 (1000000, 100000, -10000);
				state = STATE_HIDDEN;
			}
			for (int i = 0;i<renderers.Length;i++) {
				renderers[i].color = new Color (1, 1, 1, alphas[i]*alpha);
			}
			break;
		}
	}
	public StageButton AddStageButton(int place){
		stagebuttons++;
		Transform newButton = Instantiate(stageButton).transform;

		newButton.SetParent (context.transform);
		newButton.localPosition = new Vector3(0,newButton.localPosition.y,0);
		newButton.localScale = new Vector3 (1, 1, 1);
		newButton.localRotation = Quaternion.Euler( new Vector3 (0, 0, 0));
		context.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, stagebuttons * 40+10);
		newButton.GetComponentInChildren<Text> ().text = place.ToString ();
		newButton.GetComponent<RectTransform> ().offsetMin=new Vector2(0, -80-(place-1)*40);
		newButton.GetComponent<RectTransform> ().offsetMax=new Vector2(1, -50-(place-1)*40);
		return newButton.GetComponent<StageButton> ();
	}
	public void ShowUp(Transform point)
	{
		if (!pointerIn) {//if doubletapped while pointer is over buttong do not move the menu (it's annoying)
			transform.position = point.position;
			transform.rotation = point.rotation;

			foreach (Graphic renderer in renderers) {
				renderer.color = new Color (1, 1, 1, 0.0f);
			}
			state = STATE_APPEARING;
		}
	}
	public void PointerEnter(){
		pointerIn = true;
		if (state == STATE_DISAPPEARING) {
			state = STATE_APPEARING;
		}
	}
	public void PointerExit(){
		pointerIn = false;
		if (state == STATE_SHOWING) {
			countDown = 5f;
		}

	}
}
