﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PermanentUI : MonoBehaviour {

	private Text atStage;
	// Use this for initialization
	void Start () {
		atStage = GetComponentInChildren<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void SetStage(int stage, int maxStage){
		atStage.text = "Stap: " + (stage+1).ToString ()+ " van de "+(maxStage+1).ToString();
	}
}
