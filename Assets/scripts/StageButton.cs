﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class StageButton : MonoBehaviour {
	//private Text
	// Use this for initialization
	private int myStage;

	public int initButton(int stage, RoadMapHandler roadMap){
		myStage = stage;
		GetComponent<Button> ().onClick.AddListener (delegate {roadMap.StageSet(myStage);});
		transform.GetComponentInChildren<Text>().text="Stap "+(myStage+1).ToString();
		return 0;
	}
	// Update is called once per frame
	void Update () {
		
	}
}
