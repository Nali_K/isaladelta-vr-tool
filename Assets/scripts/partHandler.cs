﻿using UnityEngine;
using System.Collections;


public class PartHandler : MonoBehaviour {
	public int stage = 0; //stage at which this part is added
	public Material matNormal; // normal material when part belongs to a previous stage
	public Material matHighlight; // highlight material when part belogs to current stage

	private MeshRenderer mesh;
	// Use this for initialization
	void Awake () {
		mesh = transform.GetComponent<MeshRenderer> ();//get the renderer

	}

	public void UpdateStage(int setStage){
		if (setStage >= stage) {
			mesh.enabled = true;
			if (setStage == stage) {
				mesh.material = matHighlight;
			} else {
				mesh.material = matNormal;
			}
		} else {
			mesh.enabled = false;
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
