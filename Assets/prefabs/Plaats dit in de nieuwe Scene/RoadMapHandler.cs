﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[System.Serializable]
public class RoadMapHandler : MonoBehaviour {
	public int stage=0;//current stage showing
	public PartHandler[] parts;// array of all parts
	public Main main;
	public int maxStage; // highest stage in this object
	public UIhandler uiHanlder;
	bool canSwipe=true;
	public Material matNormal; // normal material when part belongs to a previous stage
	public Material matHighlight; // highlight material when part belogs to current stage
	// Use this for initialization
	void Start () {
		
		parts= transform.GetComponentsInChildren<PartHandler> ();//get array of all parts from children
		//update stage of all parts
		foreach (PartHandler part in parts)
		{
			part.matNormal = matNormal;
			part.matHighlight = matHighlight;

			part.UpdateStage (stage);

			//find the max stage
			if (part.stage > maxStage) {
				maxStage = part.stage;
			}

				
		}
		for  (int i=0;i<=maxStage;i++)
		{
			StageButton newButton=uiHanlder.AddStageButton(i);
			newButton.initButton(i,this);

		}
	}

	// Update is called once per frame
	void Update () {
		/*if (Input.GetAxis ("Mouse Y") > 1) {
			if (canSwipe) {
				canSwipe = false;
				StageForward ();
			}
		} else if (Input.GetAxis ("Mouse Y") < -1) {
			if (canSwipe) {
				canSwipe = false;
				StageBack ();
			}
		} else
			canSwipe = true;*/
	}

	public void StageSet(int newStage){
		stage = newStage;
		if (stage > maxStage) {
			stage = maxStage;
		}
		if (stage < 0) {
			stage = 0;
		}
		Debug.Log ("set stage=" + stage.ToString ());
		//update stage of all parts
		foreach (PartHandler part in parts)
		{
			part.UpdateStage (stage);
		}
	}
	public void AddPartHandlers(){
		for ( int i=0; i < transform.childCount;i++)
		{
			GameObject child =transform.GetChild (i).gameObject;
			if (child.GetComponent<MeshRenderer> () != null) {
				if (child.GetComponent<PartHandler> () != null) {
					DestroyImmediate (child.GetComponent<PartHandler> ());
				}
				PartHandler childPartHandler = child.AddComponent<PartHandler> () as PartHandler;
					
					childPartHandler.stage = i;



			} else {
				for (int j = 0; j < child.transform.childCount; j++) {
					GameObject subChild = child.transform.GetChild (j).gameObject;

					if (subChild.GetComponent<PartHandler> () != null) {
						DestroyImmediate (subChild.GetComponent<PartHandler> ());
					}
						PartHandler childPartHandler = subChild.AddComponent<PartHandler> ();
						childPartHandler.stage = i;
						childPartHandler.matNormal = matNormal;
						childPartHandler.matHighlight = matHighlight;


				}
			
			}
		}
	}
}
